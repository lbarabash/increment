# Abstract

A counter that is increased by 1 each time you click on a button.

# Install

git clone git@bitbucket.org:lbarabash/increment.git

cd increment

npm install

npm start

Open http://localhost:3000/ in your browser.

# Details

Based on the https://github.com/davezuko/react-redux-starter-kit boilerplate repository.

Touched files in directories:

src/layouts/PageLayout

src/components

Everything else is not touched :)
